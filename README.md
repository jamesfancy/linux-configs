# Linux Config Readme

## 使用说明

安装并初始化 [oh-my-posh](https://ohmyposh.dev/docs/installation/linux)

> 注：因为 oh-my-posh 是在用户范围内安装，所以如果 oh-my-posh 没安装，会自动安装。

```bash
curl -s https://raw.gitcode.com/jamesfancy/linux-configs/raw/main/setup-posh | bash -s
```

初始化 [micro](https://micro-editor.github.io/)

> 注：需要自行安装 micro，比如使用 apt 安装
>
> ```bash
> sudo apt update
> sudo apt install micro
> ```

```bash
curl -s https://raw.gitcode.com/jamesfancy/linux-configs/raw/main/setup-micro | bash -s
```

## 分支说明

- ⌥ `oh-my-posh` 中保存着 oh-my-posh 的配置和 `jamesfan` 主题

- ⌥ `micro` 中保存着 micro 的配置和 `zenburn-jfan` 主题

    
